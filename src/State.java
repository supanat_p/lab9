/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Interface of the states.
 * @author Supanat Pokturng
 * @version 2015.03.26
 */
public interface State {
	
	/**
	 * Check this character must go to what state.
	 * @param c is a character
	 */
	public void handleChar( char c );
}
