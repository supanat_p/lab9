/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

/**
 * Class to counter the words and syllables in the text.
 * @author Supanat Pokturng
 * @version 2015.03.26
 */
public class WordCounter {
	
	/* Initialize and declare */
	State state;
	private int count = 0;
	protected int totalSyllables = 0;
	
	/**
	 * Count the syllables of the text by using state.
	 * @param word is the text that wawnt to count
	 * @return quantity of the syllables
	 */
	public int countSyllables( String word ) {
		count = 0;
		state = startState;
		/* Check every character in each word */
		for( char c : word.toCharArray() ) {
			state.handleChar(c);
		}
		if(state == dashState){
			count = 0;
		}
		else if( state == eState && count != 1) {
			count--;
		}
			
		return count;
	}
	
	/**
	* Count all words read from a text in url and return the total count.
	* @param url is the directory to read from.
	* @return number of words in the URL.
	*/
	public int countWords(URL url) {
		int count = 0;
		try {
			InputStream in = url.openStream( );
			BufferedReader breader = new BufferedReader( new InputStreamReader(in) );
			while( true ) {
				 String line = breader.readLine( );
				 if (line == null) break;
				 totalSyllables += countSyllables(line);
				 count++;
			}
		} catch (IOException e) {
		}
		return count;
		
	}
	
	/**
	 * Check this character is the vowel or not.
	 * @param c is a character that want to check
	 * @return true if it's a vowel
	 */
	public boolean isVowel(char c) {
		String vowelList = "AEIOUaeiou";
		String temp = c+"";
		return vowelList.contains(temp);
	}
	
	/**
	 * Consonant state.
	 * come in to this state when the character is not a vowel
	 */
	State consonantState = new State() {
		/**
		 * Check the state of character.
		 * @param c is a character that want to check
		 */
		public void handleChar( char c ) {
			/* Go to E state and count the syllable */
			if( c=='e' || c=='E'){
				count++;
				state = eState;
			}
			/* Go to vowel state and count the syllable 
			 * when the character is vowel or 'y' */
			else if( isVowel(c) || c=='y' || c=='Y'){
				count++;
				state = vowelState;
			}
			/* Go to consonant state */
			else if( Character.isLetter(c) )
				state = consonantState;
			/* Go to dash state */
			else if( c=='-' )
				state = dashState;
			/* Go to consonant state when found apostrophe */
			else if( c=='\'')
				state = consonantState;
			/* Go to non-word state when found the other symbols. */
			else
				state = nonWordState;
		}
	};
	
	/**
	 * Vowel state.
	 * come in to this state when the character is a vowel
	 */
	State vowelState = new State() {
		/**
		 * Check the state of character.
		 * @param c is a character that want to check
		 */
		public void handleChar( char c ) {
			/* Go to vowel state */
			if ( isVowel(c) )
				state = vowelState;
			/* Go to consonant state */
			else if ( Character.isLetter( c ) && !(isVowel(c) ) )
				state = consonantState;
			/* Go to dash state */
			else if ( c=='-' )
				state = dashState;
			/* Go to non-word state when found the others symbols */
			else
				state = nonWordState;
		}
	};
	
	/**
	 * Dash state.
	 * come in to this state when the character is a dash (-)
	 */
	State dashState = new State() {
		/**
		 * Check the state of character.
		 * @param c is a character that want to check
		 */
		public void handleChar( char c ) {
			/* Go to vowel state and count the syllable */
			if ( isVowel(c) ) {
				count++;
				state = vowelState;
			}
			/* Go to consonant state */
			else if ( Character.isLetter( c ) )
				state = consonantState;
			/* Go to non-word state */
			else
				state = nonWordState;
		}
	};
	
	/**
	 * E state.
	 * come in to this state when the character is the letter 'e'
	 * and that is the first vowel of the word
	 */
	State eState = new State() {
		/**
		 * Check the state of character.
		 * @param c is a character that want to check
		 */
		public void handleChar( char c ) {
			/* Go to vowel state */
			if( isVowel(c) )
				state = vowelState;
			/* Go to consonant state */
			else if( Character.isLetter(c) )
				state = consonantState;
			/* Go to dash state */
			else if( c=='-' )
				state = dashState;
			/* Go to non-word state when the character is the others symbols */
			else
				state = nonWordState;
		}
	};
	
	/**
	 * Non-word state.
	 * come in to this state when the character is not a letter.
	 */
	State nonWordState = new State() {
		/**
		 * Check the state of character.
		 * @param c is a character that want to check
		 */
		public void handleChar ( char c ) {
			count = 0;
		}
	};
	
	/**
	 * Start state.
	 * come this state when start to count a syllables.
	 */
	State startState = new State() {
		/**
		 * Check the state of character.
		 * @param c is a character that want to check
		 */
		public void handleChar( char c ) {
			/* Go to E state and count the syllable when 
			 * the character is 'e' */
			if( c=='e' || c=='E') {
				count++;
				state = eState;
			}
			/* Go to vowel state and count the syllable when
			 * the character is a vowel or 'y' */
			else if( isVowel(c) || c=='y' || c=='Y' ) {
				count++;
				state = vowelState;
			}
			/* Go to consonant state */
			else if( Character.isLetter(c) )
				state = consonantState;
			/* Go to non-word state when the character is the others symbols */
			else
				state = nonWordState;
		}
	};
	
}
