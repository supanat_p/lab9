/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class to execute the program.
 * @author Supanat Pokturng
 * @version 2015.03.26
 */
public class Main {
	
	/**
	 * Execute the program.
	 * @param a is an arguments
	 */
	public static void main( String [] a) {
		/* Initialize and declare the word counter and stop-timer. */
		WordCounter wordCounter = new WordCounter();
		StopWatch timer = new StopWatch();
		/* Link of the text to count the word. */
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		try {
			/* directory of the text. */
			URL url = new URL( DICT_URL );
			/* Start timing. */
			timer.start();
			/* Show the link. */
			System.out.println(wordCounter.countWords(url));
			/* Stop timing. */
			timer.stop();
			/* Show the result. */
			System.out.println(wordCounter.totalSyllables);
			System.out.println(timer.getElapsed());
		} catch (MalformedURLException e) {
		}
	}
}
