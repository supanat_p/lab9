/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 * Class can calculate the elapsed time.
 * @author Supanat Pokturng
 * @version 2015.03.26
 */
public class StopWatch {
	
	/** Status of watch. */
	private boolean running;
	
	/** time that start and end the watch. */
	private long start = 0;
	private long end;
	
	/** constant for converting nanoseconds to seconds. */
	private static final double NANOSECONDS = 1.0E-9;
	
	/**
	 * Check status of the watch.
	 * @return true if the watch is running.
	 */
	public boolean isRunning() {
		return this.running;
	}
	
	/**
	 * Start timing.
	 */
	public void start() {
		if(!isRunning()) {
			this.running = true;
			this.start = System.nanoTime();
		}
	}
	
	/**
	 * Stop timing.
	 */
	public void stop() {
		if( isRunning() ) {
			this.running = false;
			this.end = System.nanoTime();
		}
	}
	
	/**
	 * Get the time interval between starting
	 * and ending time, or current time.
	 * @return double of elapsed time
	 */
	public double getElapsed() {
		long endTime;
		if( isRunning() ) 
			endTime = System.nanoTime();
		else
			endTime = this.end;
		return (endTime - this.start) * NANOSECONDS;
	}
}